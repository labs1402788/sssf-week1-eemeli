'use strict';
let spyArray = [];
$.ajax("js/data.json", {
    success: function(data) {
        $.each(data, function(index, item) {
            spyArray.push(item);
        });

    }, complete: function () {

            let categoriesArray = ["All"];
            for(const item in spyArray) {
                if (!categoriesArray.includes(spyArray[item].category)) {
                    categoriesArray.push(spyArray[item].category);
                }
            }

            let sortHtml ="<p>Sort by: </p>";
            for(const item of categoriesArray) {
                sortHtml += `
              <a href="#" class="btn btn-primary" id="sortBtn" onclick="sortItems(${item})">${item}</a>`;
            }
            document.getElementById("sorter").innerHTML = sortHtml;

            const wives = spyArray.filter(item=>item.category==categoriesArray[1]);
            console.log(wives);
            const gfs = spyArray.filter(item=>item.category==categoriesArray[2]);
            console.log(gfs);


            let html = "";
            for (let item in spyArray) {
                html += `<div class="col card" style="width: 20rem;">
                              <img class="card-img-top" src="${spyArray[item].thumbnail}" alt="Card image cap">
                              <div class="card-block">
                                <h4 class="card-title">${spyArray[item].title}</h4>
                                <p class="card-text">${spyArray[item].time}</p>
                                <p class="card-text">${spyArray[item].details}</p>
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="openModal(`+ item +`);">View</a>
                              </div>
                            </div>`;
            }

            document.getElementById("thumb").innerHTML = html;

            function openModal(index) {
                let modalHtml ="";
                modalHtml += `<div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">${spyArray[index].title}</h4>
                 </div>
                 <div class="modal-body">
                    <img src="${spyArray[index].image}">
                 </div>
                 <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 </div>`;

                document.getElementById("theModal").innerHTML = modalHtml;
            }
    }
});
